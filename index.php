<?php require_once"./code.php"; ?>

<h1>Divisible of Five</h1>
	
	<?php divisibleByFive(); ?>


<h1>Array Manipulation</h1>

	<?php array_push($students, 'John Smith'); ?>
	<pre><?php var_dump($students); ?></pre>
	<pre><?php echo count($students); ?></pre>

	<?php array_push($students, 'Jane Smith'); ?>
	<pre><?php var_dump($students); ?></pre>
	<pre><?php echo count($students); ?></pre>

	<?php array_shift($students); ?>
	<pre><?php var_dump($students); ?></pre>
	<pre><?php echo count($students); ?></pre>


